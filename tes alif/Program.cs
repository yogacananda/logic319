﻿
//pemulung();
//ganjilgenap();
//modulus();
//Persegi();
//GradeNilai();
Lingkaran();
Console.ReadKey();



static void Lingkaran()
{
    Console.WriteLine("LINGKARAN");


    Console.Write("masukan jari jari lingkaran : ");
    double r = Convert.ToDouble(Console.ReadLine());

    double luas = LuasLingkaran(r);
    double keliling = KelilingLingkaran(r);

    Console.WriteLine("luas lingkaran: " + luas);
    Console.WriteLine("keliling lingkaran " + keliling);

}

static double LuasLingkaran(double r)
{

    double luas = Math.PI * r * r;
    return luas;
}

static double KelilingLingkaran(double r)
{
    double keliling = 2 * Math.PI * r;
    return keliling;
}

static void Persegi()
{
    Console.WriteLine("--Persegi--");
    Console.Write("Masukkan panjang sisi: ");
    double sisi = Convert.ToDouble(Console.ReadLine());

    double luas = HitungLuasPersegi(sisi);
    double keliling = HitungKelilingPersegi(sisi);

    Console.WriteLine("Luas persegi: " + luas);
    Console.WriteLine("Keliling persegi: " + keliling);

    Console.ReadLine();
}

static double HitungLuasPersegi(double sisi)
{
    return sisi * sisi;
}

static double HitungKelilingPersegi(double sisi)
{
    return 4 * sisi;
}


static void GradeNilai()
{
    Console.WriteLine("GRADE NILAI");
    Console.Write("masukin nilai: ");
    int nilai = int.Parse(Console.ReadLine());
    string grade = TentukanGrade(nilai);
    Console.Write($"grade : {grade}");
}

static string TentukanGrade(int nilai)
{
    if (nilai >= 80  && nilai > 100 )
    {
        return "A";
    }
    else if (nilai >= 60 && nilai < 80)
    {
        return "B";
    }
    else
    {
        return "C";
    }
}


static void modulus()
{
    Console.WriteLine("Cari Modulus 0");
    Console.WriteLine("---------------------");

    Console.Write("Masukkan angka awal : ");
    int angka = Convert.ToInt32(Console.ReadLine());

    Console.Write("Masukkan pembagi: ");
    int pembagi = Convert.ToInt32(Console.ReadLine());
    int hasil = angka % pembagi;

    if (angka % pembagi == 0)
    {
        Console.WriteLine("Hasil modulus " + angka + " dengan " + pembagi + " adalah 0");
    }
    else
    {
        Console.WriteLine("Hasil modulus " + angka + " dengan " + pembagi + " bukan 0 melainkan hasil mod " +hasil );
    }

    Console.ReadLine();
}


static void ganjilgenap()
{
    Console.WriteLine("Program Mengecek Bilangan Ganjil atau Genap");
    Console.WriteLine("-----------------------------------------");

    Console.Write("Masukkan sebuah bilangan: ");
    int bilangan = Convert.ToInt32(Console.ReadLine());

    if (bilangan % 2 == 0)
    {
        Console.WriteLine(bilangan + " adalah bilangan genap");
    }
    else
    {
        Console.WriteLine(bilangan + " adalah bilangan ganjil");
    }

    Console.ReadLine();
}



static void pemulung()
{
    Console.WriteLine("Pemulung");
    Console.WriteLine("----------------");

    Console.Write("Masukkan sebuah bilangan: ");
    int bilangan = Convert.ToInt32(Console.ReadLine());

    int hasilModulus = bilangan % 8;
    int rokok = bilangan * 500;

    Console.WriteLine("Hasil rokok yang berhasil dirangkai adalah " + hasilModulus + " dan uang yang didapat adalah " + rokok);

    Console.ReadLine();
}
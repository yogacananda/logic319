﻿
//Tugas1();
//Tugas5();
//Tugas3();
//Tugas8();
//Tugas2();
//Tugas4();
//Tugas6();
Tugas7();

static void Tugas7()
{
    
    Console.Write("Masukkan Berat Badan Anda (kg) \t : ");
    float berat = float.Parse(Console.ReadLine());

    Console.Write("Masukkan Tinggi Badan Anda (cm) \t : ");
    float tinggi = float.Parse(Console.ReadLine());

    float bmi, height;
    height = (tinggi / 100) * (tinggi / 100);

    bmi = berat / height;

    Console.WriteLine($"Nilai BMI anda adalah \t : {bmi}");

    if (bmi < 18.5)
    {
        Console.WriteLine("kurus sekali!");
    }
    else if (bmi > 18.5 && bmi < 25)
    {
        Console.WriteLine("agak langsing lah ya!");
    }
    else if (bmi >= 25)
    {
        Console.WriteLine("Gemuk sekali!");
    }
    else
    {

    }
}


static void Tugas6()
{
    
    Console.Write("Nama \t : ");
    string nama = Console.ReadLine();
    Console.Write("Tunjangan \t : ");
    int tunj = int.Parse(Console.ReadLine());
    Console.Write("Gapok \t : ");
    int gap = int.Parse(Console.ReadLine());
    Console.Write("Banyak Bulan \t : ");
    int moon = int.Parse(Console.ReadLine());

    int galan, totga, gaji;
    double pajak, bpjs, gbln, gtot;
    gaji = tunj + gap;

    if (gaji <= 5000000)
    {
        pajak = 0.05 * gaji;
        bpjs = 0.03 * gaji;
        gbln = (gap + tunj) - (pajak + bpjs);
        gtot = gbln * moon;

        Console.WriteLine($"Karyawan atas nama {nama} slip gaji sebagai berikut : ");
        Console.WriteLine($"Pajak \t Rp. {pajak} ");
        Console.WriteLine($"Bpjs \t Rp. {bpjs} ");
        Console.WriteLine($"Gaji/Bulan \t Rp. {gbln} ");
        Console.WriteLine($"Total Gaji \t Rp. {gtot} ");
    }
    else if (gaji > 5000000 && gaji <= 10000000)
    {
        pajak = 0.10 * gaji;
        bpjs = 0.03 * gaji;
        gbln = (gap + tunj) - (pajak + bpjs);
        gtot = gbln * moon;

        Console.WriteLine($"Karyawan atas nama {nama} slip gaji sebagai berikut : ");
        Console.WriteLine($"Pajak \t Rp. {pajak} ");
        Console.WriteLine($"Bpjs \t Rp. {bpjs} ");
        Console.WriteLine($"Gaji/Bulan \t Rp. {gbln} ");
        Console.WriteLine($"Total Gaji \t Rp. {gtot} ");
    }
    else if (gaji > 10000000)
    {
        pajak = 0.15 * gaji;
        bpjs = 0.03 * gaji;
        gbln = (gap + tunj) - (pajak + bpjs);
        gtot = gbln * moon;

        Console.WriteLine($"Karyawan atas nama {nama} slip gaji sebagai berikut : ");
        Console.WriteLine($"Pajak \t Rp. {pajak} ");
        Console.WriteLine($"Bpjs \t Rp. {bpjs} ");
        Console.WriteLine($"Gaji/Bulan \t Rp. {gbln} ");
        Console.WriteLine($"Total Gaji \t Rp. {gtot} ");
    }
    else
    {
        Console.WriteLine("Invalid!!");
    }
}


static void Tugas4()
{
    int input_price, input_shipping, input_voucher;

    Console.Write("harga belanja\t: ");
    input_price = int.Parse(Console.ReadLine());
    Console.Write("ongkir\t: ");
    input_shipping = int.Parse(Console.ReadLine());
    Console.Write("pilih voucher (1/2/3)\t: ");
    input_voucher = int.Parse(Console.ReadLine());

    switch (input_voucher)
    {
        case 1:

            int free_cost1 = 5000;
            int discount1 = 5000;
            int total_cost1 = input_price + input_shipping - free_cost1 - discount1;

            if (input_price >= 30000)
            {
                Console.WriteLine();
                Console.WriteLine($"Total Shopping\t: {input_price}");
                Console.WriteLine($"Total Shopping\t: {input_shipping}");
                Console.WriteLine($"Total Shopping\t: {free_cost1}");
                Console.WriteLine($"Total Shopping\t: {discount1}");
                Console.WriteLine($"Total Shopping\t: {total_cost1}");
            }
            else
            {
                Console.WriteLine("ga punya voucher!!!!");
            }
            break;

        case 2:

            int free_cost2 = 10000;
            int discount2 = 10000;
            int total_cost2 = input_price + input_shipping - free_cost2 - discount2;

            if (input_price >= 50000)
            {
                Console.WriteLine();
                Console.WriteLine($"Total Shopping\t: {input_price}");
                Console.WriteLine($"Total Shopping\t: {input_shipping}");
                Console.WriteLine($"Total Shopping\t: {free_cost2}");
                Console.WriteLine($"Total Shopping\t: {discount2}");
                Console.WriteLine($"Total Shopping\t: {total_cost2}");

            }
            else
            {
                Console.WriteLine("ga punya voucher!!!!");
            }

            break;

        case 3:
            int free_cost3 = 20000;
            int discount3 = 10000;
            int total_cost3 = input_price + input_shipping - free_cost3 - discount3;

            if (input_price >= 100000)
            {
                Console.WriteLine();
                Console.WriteLine($"Total Shopping\t: {input_price}");
                Console.WriteLine($"Total Shopping\t: {input_shipping}");
                Console.WriteLine($"Total Shopping\t: {free_cost3}");
                Console.WriteLine($"Total Shopping\t: {discount3}");
                Console.WriteLine($"Total Shopping\t: {total_cost3}");

            }
            else
            {
                Console.WriteLine("ga punya voucher!!!!");
            }

            break;

        default:
            Console.WriteLine("salah pilih, silakan pilih voucher lagi 1/2/3)");
            break;
    }
}
Tugas4();



static void Tugas8()
{
    Console.WriteLine("Rata-Rata");
    Console.WriteLine();
    Console.Write("Masukan nilai Mtk\t: ");
    int mtk = int.Parse(Console.ReadLine());
    Console.Write("Masukkan nilai fisika\t: ");
    int fisika = int.Parse(Console.ReadLine());
    Console.Write("Masukkan nilai kimia\t: ");
    int kimia = int.Parse(Console.ReadLine());
    int rata = (mtk + fisika + kimia) / 3;
    if (rata >= 50 && rata <= 100)
    {
        Console.WriteLine($"nilai rata rata : {rata}");
        Console.WriteLine("Selamat kamu hoki");

    }
    else
    {
        Console.WriteLine($"nilai rata rata : {rata}");
        Console.WriteLine("Maaf coba lagi");
    }
}


static void Tugas3()

{
    Console.Write("Masukkan total pembelanjaan: ");
    double totalPembelanjaan = Convert.ToDouble(Console.ReadLine());

    Console.Write("Masukkan jarak pengiriman dalam km: ");
    int jarakPengiriman = Convert.ToInt32(Console.ReadLine());

    double diskon = HitungDiskon(totalPembelanjaan);
    double ongkosKirim = HitungOngkosKirim(jarakPengiriman);

    double totalPembayaran = totalPembelanjaan - diskon + ongkosKirim;

    Console.WriteLine("Total pembayaran: " + totalPembayaran);
}

static double HitungDiskon(double totalPembelanjaan)
{
    double diskon = 0;

    if (totalPembelanjaan >= 30000)
    {
        diskon = Math.Min(totalPembelanjaan * 0.4, 30000);
    }
    Console.WriteLine("diskon belanja : " + totalPembelanjaan);
    return diskon;

}

static double HitungOngkosKirim(int jarakPengiriman)
{
    int jarakMin = 5;
    int biayaOngkosKirim = 5000;
    int tambahanPerKm = 1000;

    int jarakLebih = Math.Max(jarakPengiriman - jarakMin, 0);
    double ongkosKirim = biayaOngkosKirim + (tambahanPerKm * jarakLebih);

    return ongkosKirim;
}


static double Tugas2(double jumlahPulsa)
{
    double poin = 0;

    if (jumlahPulsa >= 10000)
    {
        poin = 80;
    }
    else if (jumlahPulsa >= 25000)
    {
        poin = 200;
    }
    else if (jumlahPulsa >= 50000)
    {
        poin = 400;
    }
    else if (jumlahPulsa >= 100000)
    {
        poin = 800;
    }
    else if (jumlahPulsa < 10000)
    {
        Console.WriteLine("Pembelian pulsa harus minimal 10.000 untuk mendapatkan poin.");
    }
    else
    {
        Console.WriteLine("Pulsa tidak memenuhi kriteria untuk mendapatkan poin.");
    }

    return poin;
}

//static void Tugas3()

//{
//    const int MINIMUM_BELANJA = 30000;
//    const int ONGKOS_KIRIM_PER_5KM = 5000;
//    const double MAX_DISKON_BELANJA = 30000;
//    const double DISKON_BELANJA = 0.4;
//    const string KODE_PROMO = "JKTOVO";

//    Console.WriteLine("Masukkan total belanja (dalam Rupiah): ");
//    int totalBelanja = Convert.ToInt32(Console.ReadLine());

//    if (totalBelanja < MINIMUM_BELANJA)
//    {
//        Console.WriteLine("Maaf, total belanja tidak mencapai minimum untuk mendapatkan diskon.");
//        return;
//    }

//    Console.WriteLine("Masukkan jarak pengiriman (dalam KM): ");
//    int jarakKirim = Convert.ToInt32(Console.ReadLine());


//    double ongkosKirim = 0;

//    if (jarakKirim <= 5)
//    {
//        ongkosKirim = ONGKOS_KIRIM_PER_5KM;
//    }
//    else
//    {
//        int jarakLebih = jarakKirim - 5;
//        ongkosKirim = ONGKOS_KIRIM_PER_5KM + (jarakLebih * 1000);
//    }

//    double diskonBelanja = 0;

//    if (totalBelanja >= MINIMUM_BELANJA && totalBelanja < 100000)
//    {
//        diskonBelanja = totalBelanja * DISKON_BELANJA;
//        if (diskonBelanja > MAX_DISKON_BELANJA)
//        {
//            diskonBelanja = MAX_DISKON_BELANJA;
//        }
//    }

//    double totalPembayaran = totalBelanja - diskonBelanja + ongkosKirim;

//    Console.WriteLine("Harga Belanja: " + totalBelanja);
//    Console.WriteLine("Ongkos Kirim: " + ongkosKirim);
//    Console.WriteLine("Diskon Ongkir: " + ongkosKirim);
//    Console.WriteLine("Diskon Belanja: " + diskonBelanja);
//    Console.WriteLine("Total Belanja: " + totalPembayaran);
//}


    static void Tugas5()
{
    Console.Write("masukin nama : ");
    string nama = Console.ReadLine();

    Console.Write("masukin tahun kelahiran : ");
    int lahir = Convert.ToInt32(Console.ReadLine());

    string gen;

    if (lahir >= 1944 && lahir <= 1964)
    {
        gen = "Baby Boomer";
    }
    else if (lahir >= 1965 && lahir <= 1979)
    {
        gen = "GEN X";
    }
    else if (lahir >=1980 && lahir <= 1994)
    {
        gen = "GEN Y";
    }
    else if (lahir >= 1995 && lahir <= 2015)
    {
        gen = "GEN Z";
    }
    else
    {
        gen = "BOCIL KEMATIAN";
    }
    Console.WriteLine("nama : " + nama);
    Console.Write("generasi : " + gen);
}







static void Tugas1()
{
        Console.WriteLine("masukan nilai : ");
        int nilai = Convert.ToInt32(Console.ReadLine());

        string grade;

        if (nilai >= 90 && nilai <= 100)
        {
            grade = "A";
        }
        else if (nilai >= 70 && nilai <= 89)
        {
            grade = "B";
        }
        else if (nilai >= 50 && nilai <= 69)
        {
            grade = "C";
        }
        else if (nilai >= 0 && nilai <= 49)
        {
            grade = "E";
        }
        else
        {
            grade = "masukin nilai yg bener dong!!";

        }

        Console.WriteLine("SELAMAT GRADE ANDA ADALAH :  " + grade);
}
        
    
﻿using logic04;

//InitializeArray();
//AccessElementsArray();
//Array2Dimension();
//InitializeList();
//ClassStudent();
//AccessElementsList();
//InsertList();
//RemoveList();
//IndexElementsList();
//InitializeDateTime();
//InitializeDateTime();
Parsing();


static void Parsing()
{
    Console.WriteLine("-== DATE TIME PARSING ==-");
    string dateString = "06/30/2023";
    DateTime date = DateTime.Parse(dateString);
    Console.WriteLine(date);
}


static void InitializeDateTime()
{

    Console.WriteLine("-== INITIALIZE DATE TIME ==-");
    DateTime dt1 = new DateTime(); //01/01/0001 00:00:00
    Console.WriteLine(dt1);

    DateTime dtNow = DateTime.Now; //Date and Time now
    Console.WriteLine(dtNow);

    DateTime dt2 = new DateTime(2023, 6, 2);
    Console.WriteLine(dt2);

    DateTime dt3 = new DateTime(2020, 6, 2, 13, 20, 48);
    Console.WriteLine(dt3);
}

static void IndexElementsList()
{
    Console.WriteLine("-== INDEX ELEMENTS LIST ==-");
    //List<int> list = new List<int>() { 1, 2, 3};
    List<int> list = new List<int>();
    list.Add(1); //Index 0
    list.Add(2); //Index 1
    list.Add(3); //Index 2
    list.Add(3); //Index 2

    int item = 3;

    int index = list.IndexOf(item);

    if (index != -1)
    {
        Console.WriteLine($"Elements {item} is found at Index {index}");
    }

}


//Remove List
static void RemoveList()
{
    Console.WriteLine("-== Remove LIST ==-");
    //List<int> list = new List<int>() { 1, 2, 3};
    List<int> list = new List<int>();
    list.Add(1); //Index 0
    list.Add(2); //Index 1
    list.Add(3); //Index 2
    list.Add(3); //Index 2

    list.Remove(0); //Remove with Values
    list.RemoveAt(2); //Remove with Index

    for (int i = 0; i < list.Count; i++)
    {
        Console.WriteLine(list[i]);
    }
}


//Insert List
static void InsertList()
{
    Console.WriteLine("-== INSERT LIST ==-");
    //List<int> list = new List<int>() { 1, 2, 3};
    List<int> list = new List<int>();
    list.Add(1);
    list.Add(2);
    list.Add(3);

    list.Insert(2, 4);

    for (int i = 0; i < list.Count; i++)
    {
        Console.WriteLine(list[i]);
    }
}


//Access Elements List
static void AccessElementsList()
{
    Console.WriteLine("-== ACCESS ELEMENTS LIST ==-");
    //List<int> list = new List<int>() { 1, 2, 3};
    List<int> list = new List<int>();
    list.Add(1);
    list.Add(2);
    list.Add(3);

    Console.WriteLine(0);
    Console.WriteLine(1);
    Console.WriteLine(2);

    foreach (int i in list)
    {
        Console.WriteLine(i);
    }

    for (int i = 0; i < list.Count; i++)
    {
        Console.WriteLine(list[i]);
    }

    Console.WriteLine(String.Join(" , ", list));
}


static void ClassStudent()
{
    Console.WriteLine("-== CALL CLASS STUDENT ==-");
    List<Student> students = new List<Student>()
    {
        new Student(){Id = 1, Name = "John Doe"},
        new Student(){Id = 2, Name = "Jane Doe"},
        new Student(){Id = 3, Name = "Joe Doe"},

    };
    Console.WriteLine($"The length of the Student data list = {students.Count}");
    foreach (Student student in students)
    {
        Console.WriteLine($"Id : {student.Id} , Name : {student.Name}");
    }
}
static void InitializeList()
{

    Console.WriteLine("-== Initialize List ==-");
    List<string> list = new List<string>()
    {
        "John Doe",
        "Jane Doe",
        "Joe Doe"
    };
    list.Add("Joko Doe");

    Console.WriteLine(String.Join(" , ", list));
}

static void Array2Dimension()
{
    int[,] array = new int[3, 3]
    {
        { 1, 2, 3 },
        { 4, 5, 6 },
        { 6, 7, 8 }
    };

    for (int i = 0; i < array.GetLength(0); i++) // rows
    {
        for (int j = 0; j < array.GetLength(1); j++) //Columns
        {
            Console.WriteLine($"{array[i, j]}");
        }
        Console.WriteLine();
    }
}

static void AccessElementsArray()
{

    Console.WriteLine("-== ACCESS ELEMENTS ARRAY ==-");
    int[] intStaticArray = new int[3];
    intStaticArray[0] = 1;
    intStaticArray[1] = 2;
    intStaticArray[2] = 3;

    Console.WriteLine(intStaticArray[0]);
    Console.WriteLine(intStaticArray[1]);
    Console.WriteLine(intStaticArray[2]);

    int[] array = { 2, 4, 6 };
    for (int i = 0; i < array.Length; i++)
    {
        Console.WriteLine(array[i]);
    }
    string[] strArray = new string[]
    {
        "Fachri Kusnaini",
        "Banu Ridwan"
    };

    foreach (string str in strArray)
    {
        Console.WriteLine(str);
    }
}

static void InitializeArray()
{

    Console.WriteLine("-== Initialize Array ==-");
    //Tutorial 1
    int[] array = new int[5];
    array = new int[] { 1, 2, 3, 4, 5 };

    for (int i = 0; i < array.Length; i++)
    {
        Console.Write($"Insert your data {i + 1}\n");
        array[i] = int.Parse(Console.ReadLine());
    }
    //Tutorial 2
    int[] array2 = new int[5] { 1, 2, 3, 4, 5 };
    //Tutorial 3 
    int[] array3 = new int[] { 1, 2, 3, 4, 5 };
    //Tutorial 4
    int[] array4 = { 1, 2, 3, 4, 5, 6 };
    //Tutorial 5
    int[] array5;
    array5 = new int[] { 1, 2, 3, 4, 5 };

    Console.WriteLine(String.Join(" , ", array));
    Console.WriteLine(String.Join(" , ", array2));
    Console.WriteLine(String.Join(" , ", array3));
    Console.WriteLine(String.Join(" , ", array4));
    Console.WriteLine(String.Join(" , ", array5));
}
﻿
//Tugas2();
//Tugas3();
//Tugas5();
Tugas4();


Console.ReadKey();

static void Tugas4()
{
    Console.WriteLine("masukin kalimat : ");
    string kalimat = Console.ReadLine();

    string hasil = GantiKata(kalimat);

    Console.WriteLine("hasil : " + hasil);

}
static string GantiKata(string kalimat)
{
    string[] kata = kalimat.Split(' ');
    string hasil = "";

    for (int i = 0; i < kata.Length; i++)
    {
        string kataBaru = "";

        for (int j = 0; j < kata[i].Length; j++)
        {
            if (j == 0 || j == kata[i].Length - 1)
            {
                kataBaru += "*"; 
            }
            else
            {
                kataBaru += kata[i][j];
            }
        }
        hasil += kataBaru + " ";
    }
    return hasil.Trim();
}


static void Tugas5()

{
 
    {
        Console.WriteLine("Masukkan kalimat: ");
        string kalimat = Console.ReadLine();

        string hasil = HapusHurufPertama(kalimat);

        Console.WriteLine("Hasil: " + hasil);

        Console.ReadLine();
    }

    static string HapusHurufPertama(string kalimat)
    {
        string[] kata = kalimat.Split(' ');
        string hasil = "";

        for (int i = 0; i < kata.Length; i++)
        {
            string kataBaru = "";

            if (kata[i].Length > 1)
            {
                kataBaru = kata[i].Substring(1);
            }

            hasil += kataBaru + " ";
        }

        return hasil.Trim();
    }
}

static void Tugas3()
{
    Console.WriteLine("masukin kalimat : ");
    string kalimat = Console.ReadLine();

    string hasil = GantiHuruf(kalimat);

    Console.WriteLine("hasil : " + hasil);

}
static string GantiHuruf(string kalimat)
{
    string[] kata = kalimat.Split(' ');
    string hasil = "";

    for (int i = 0; i < kata.Length; i++)
    {
        string kataBaru = "";

        for (int j = 0; j < kata[i].Length; j++)
        {
            if (j == 0 || j == kata[i].Length - 1)
            {
                kataBaru += kata[i][j];
            }
            else
            {
                kataBaru += "*";  
            }
        }
        hasil += kataBaru + " ";
    }
    return hasil.Trim();
}




static void Tugas2()
{
    Console.WriteLine("masukin kalimat : ");
    string kalimat = Console.ReadLine();

    string[] kata = kalimat.Split(' ');

    Console.WriteLine("daftar kata : ");
    for (int i = 0; i < kata.Length; i++)
    {
        Console.WriteLine("kata " + (i + 1) + ": " + kata[i]);
    }

    Console.WriteLine("total kata : " + kata.Length);
}
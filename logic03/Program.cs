﻿
//PerulanganWhile();
//PerulanganWhile2();
//PerulanganDoWhile();
//PerulanganFor();
//Break();
//Continue();
//ForNested();
//TOCharArray();
ConvertAll();
Console.ReadKey();

static void ConvertAll()
{
    Console.Write("masukan angka : ");
    string[] input = Console.ReadLine().Split(" ");

    int sum = 0;

    int[] array = Array.ConvertAll(input, int.Parse);
    foreach (int x in array) 
    {
        sum += x;
    }
    Console.WriteLine($"jumlah = {sum}");


}






//static void ConvertStringArrayToInt()
//{
//    Console.Write("masukan angka : ");
//    string input = Console.ReadLine();
    
//    int sum = 0;    

//    string[] array = input.Split(" ");
//    foreach (string str in array) ;
//    {
//        sum += int.Parse(string);
//    }
//    Console.WriteLine($"jumlah = {sum}");
    

//}


static void TOCharArray()
{
    Console.Write("masukan kalimat : ");
    string kalimat = Console.ReadLine();

    char[] array = kalimat.ToCharArray();

    foreach(char x in array)
    {
        Console.Write(x);
    }
    Console.WriteLine();

    for (int i = 0; i < array.Length; i++)
    {
        Console.WriteLine(array[i]);
    }
}





static void ForNested()
{
    for (int i = 0; i < 5; i++)
    {
        for (int j = 0; j < 5; j++)
        {
            Console.Write($"({i} , {j})");
        }
        Console.WriteLine("\n");
    }
}




static void Continue()
{
    for (int i = 0; i < 10; i++)
    {
        if(i == 7)
        {
            continue;
        }
        Console.WriteLine(i);
    }
}





static void Break()
{
    for (int i = 0; i < 10; i++)
    {
        
        if (i == 5)
        {
            break;
        }
        Console.WriteLine(i);
    }
}




static void PerulanganFor()
{
    for (int i = 0; i < 6; i++)
    {
        Console.Write( "\t\n" + i);
    }

    Console.WriteLine("\n");

    for (int i = 10; i >= 0; i--)
    {
        Console.WriteLine(i);
    }
}






static void PerulanganDoWhile()
{
    Console.Write("masukin nilai : ");
    int nilai = int.Parse(Console.ReadLine());

    do
    {
        Console.WriteLine(nilai);
        nilai++;
    } while (nilai < 6);
}





static void PerulanganWhile2()
{
    bool ulangi = true;

    Console.WriteLine("masukin nilai : ");
    int nilai = int.Parse(Console.ReadLine());

    while (ulangi)
    { 
        Console.WriteLine($"proses ke {nilai}");
        nilai++;

        Console.Write("ulangi proses? (y/n) : ");
        string input = Console.ReadLine();

        if (input.ToLower() == "n")
        {
            ulangi = false;
        }
    }
}




static void PerulanganWhile()
{
    Console.WriteLine("Perulangan While");
    Console.Write("masukin nilai : ");
    int nilai = int.Parse(Console.ReadLine());

    while (nilai < 6);
    {
        Console.WriteLine(nilai);
        nilai++;
    }

}
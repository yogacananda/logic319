﻿

//IfStatment();
//IfElseIf();
//IfNested();
//Ternary();
using System.Diagnostics;

Console.ReadKey();


static void Switch()
{
    Console.WriteLine("pilih buah (pisang, apel, jeruk");
    string pilihan = Console.ReadLine().ToLower();
    
    switch (pilihan)
    {
        case "pisang":
            Console.WriteLine("anda pilih pisang");
            break;
        case "apel":
            Console.WriteLine("anda pilih apel");
            break ;
        case "jeruk":
            Console.WriteLine("anda pilih jeruk");
            break;
    }
}



static void Ternary()
{

    Console.Write("masukan nilai x : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("masukan nilai y : ");
    int y = int.Parse(Console.ReadLine());
   // var x = 30;
    //var y = 99;
    string z;

    z = (y > x) ? "y lebih gede dari x" : "x lebih besar dari y";
    Console.WriteLine(z);

}


static void IfNested()
{
    Console.Write("masukin nilai : ");
    int nilai = int.Parse(Console.ReadLine());


    if (nilai <= 50)
    {
        Console.WriteLine("hoki");

        if (nilai == 80)
        {
            Console.WriteLine("bisa lah ya");
        }
    }
    else
    {
        Console.WriteLine("bodoooh");
    }
}




static void IfElseIf()
{
    Console.Write("masukin x : ");
    int x = int.Parse(Console.ReadLine());

    if(x == 10)
    {
        Console.WriteLine("x sama 10");
    }
    else if(x >10)
    {
        Console.WriteLine("x lebih dari 10");
    }
    else
    {
        Console.WriteLine("x kurang dari 10");
    }
}


















static void IfStatment()
{
    Console.Write("masukan X : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("masukan Y : ");
    int y = int.Parse(Console.ReadLine());

    if (x >= 20)
    {
        Console.WriteLine("x lebih gede dari 20");
    }
    else 
    { Console.WriteLine("x kurang woi");
    }
    if (y <= 8)
    {
        Console.WriteLine(" y lebih kecil dari 8");
    }
}
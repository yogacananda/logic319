﻿
//Tugas1();
//Tugas2();
//Tugas3();
//Tugas5();
//Tugas6();
Tugas4();
Console.ReadKey();

static void Tugas4()
{
    Console.WriteLine("PEMULUNG");
    Console.Write("masukan putungan rokok: ");
    int pr = int.Parse(Console.ReadLine());        //pr = putung rokok
    int br = pr / 8;    //br = batang rokok
    int sr = pr % 8;    //sr = sisa rokok

    Console.WriteLine(" jumlah rokok rakitan : " + br);
    Console.WriteLine("sisa putung rokok: " + sr);

    int hargaJual = 500;
    int penghasilan = br * hargaJual;

    Console.WriteLine("kekayaan pemulung : Rp. " + penghasilan);
}





static void Tugas6()
{
    Console.WriteLine("GANJIL GENAP");
    Console.Write("masukan angka: ");
    int angka = int.Parse(Console.ReadLine());

    if (angka % 2 == 0)
    {
        Console.WriteLine("bilangan genap");
    }
    else
    {
        Console.WriteLine("bilangan ganjil");
    }
}




static void Tugas5()
{
    Console.WriteLine("GRADE NILAI");
    Console.Write("masukin nilai: ");
    int nilai = int.Parse(Console.ReadLine());
    string grade = TentukanGrade(nilai);
    Console.Write($"grade : {grade}");
}

static string TentukanGrade(int nilai)
{
    if (nilai >= 80 && nilai > 100)
    {
        return "A";
    }
    else if (nilai >= 60 && nilai < 80)
    {
        return "B";
    }
    else
    {
        return "C";
    }
}







static void Tugas3()
{
    Console.Write("masukan angka : ");
    int angka = int.Parse(Console.ReadLine());

    Console.Write("masukan pembagi : ");
    int pembagi = int.Parse(Console.ReadLine());

    if (angka % pembagi == 0)
    {
        Console.WriteLine($"{angka} % {pembagi} adalah 0");
    }
    else
    {
        Console.WriteLine($"{angka} % {pembagi} bukan 0 tapi sisanya {angka % pembagi}");
    }
}





static void Tugas2()
{
    Console.WriteLine("PERSEGI");

    float p, lb, ls, keliling;
    Console.Write("masukan panjang : ");
    p = float.Parse(Console.ReadLine());
    Console.Write("masukan lebar : ");
    lb = float.Parse(Console.ReadLine()); //lebar
    ls = p * lb; //luas
    keliling = 2 * p + 2 * lb;
    Console.WriteLine($"luasnya {ls}");
    Console.WriteLine($"keliling {keliling}"); 

}







static void Tugas1()
{
    Console.WriteLine("LINGKARAN");


    Console.Write("masukan jari jari lingkaran : ");
    double r = Convert.ToDouble(Console.ReadLine());

    double luas = LuasLingkaran(r);
    double keliling = KelilingLingkaran(r);

    Console.WriteLine("luas lingkaran: " + luas);
    Console.WriteLine("keliling lingkaran " + keliling);

}

static double LuasLingkaran(double r)
{
    
    double luas = Math.PI * r * r;
    return luas;
}

static double KelilingLingkaran(double r)
{
    double keliling = 2 * Math.PI * r;
    return keliling;
}